const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const path = require("path");
const exphbs = require("express-handlebars");

//inicializar
const app = express();

// Settings
app.set("port", process.env.PORT || 3000);

app.set("views", path.join(__dirname, "views"));
app.engine(".hbs", exphbs.engine({
  defaultLayout: "main",
  layoutDir: path.join(app.get("views"), "layouts"),
  extname: ".hbs"
}));
app.set("view engine", ".hbs");


// Middlewares
// const corsOptions = {origin: "http://localhost:4200"}
app.use(cors());
app.use(morgan("dev"));
app.use(express.json()); 

// Routes
app.use("/api/employees", require("./routes/employee.routes"));

module.exports = app;