const Employee = require("../models/employee");

const employeeCtrl = {};

employeeCtrl.getEmployees = async (req, res, next) => {
    const employees = await Employee.find();
    //res.json(employees);    
    //envia los datos del empleado
    let arrayMongoEmployees = employees;
    res.render("./views/employee_list",  arrayMongoEmployees);
};


employeeCtrl.createEmployee = async (req, res, next) => {
    const employee = new Employee({
      name: req.body.name,
      position: req.body.position,
      office: req.body.office,
      salary: req.body.salary,
    });
    await employee.save();
    res.json({ status: "Employee created" });
  };
  
  module.exports = employeeCtrl;