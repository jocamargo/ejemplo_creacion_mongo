const express = require("express");
const router = express.Router();

const employee = require("../controllers/employee.controller");

//retrieve
router.get("/", employee.getEmployees);

//create
router.post("/", employee.createEmployee);

//retrieve by id
//router.get("/:id", employee.getEmployee);

//update
//router.put("/:id", employee.editEmployee);

//delete
//router.delete("/:id", employee.deleteEmployee);

module.exports = router;